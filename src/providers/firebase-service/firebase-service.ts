import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';

@Injectable()
export class FirebaseService{

  constructor(public afd: AngularFireDatabase){ }

  getUser(){
    return this.afd.list('/users/');
  }

  pushUser(email, password){
    this.afd.list('/users/').push(email, password);
  }

  removeUser(id){
    this.afd.list('/users/').remove(id);
  }

}
