import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { FirebaseService } from '../../providers/firebase-service/firebase-service';
import { AngularFireAuth } from 'angularfire2/auth';

import { User } from '../../models/user';

import { LoginPage } from '../login/login';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  user = {} as User;

  constructor(private afauth: AngularFireAuth, public navCtrl: NavController, public navParams: NavParams, public firebaseService: FirebaseService) {

  }

  async addUser(user: User){
    try{
      const result = await this.afauth.auth.createUserWithEmailAndPassword( user.email, user.password)
      this.firebaseService.pushUser(this.user);
      this.navCtrl.push(LoginPage);
      console.log(result);
    }catch (e){
      console.error(e);
    }
  }

  deletUser(id){
    this.firebaseService.removeUser(id);
  }

}
