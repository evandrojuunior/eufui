import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';

import { FirebaseService } from '../../providers/firebase-service/firebase-service';
import { AngularFireAuth } from 'angularfire2/auth';

import { User } from '../../models/user';

import { SignupPage } from '../signup/signup';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  user = {} as User;

  constructor(public navCtrl: NavController, public firebaseService: FirebaseService, private afauth: AngularFireAuth, public menu: MenuController) {
    this.menuInactive();
  }

  async Login(user: User){
    try{
      const result = this.afauth.auth.signInWithEmailAndPassword(user.email, user.password);
      console.log(result);
      this.menuActive();
      this.navCtrl.setRoot(HomePage);
    }catch(e){
      console.error(e);
    }
  }

  ionViewDidLoad(){
    this.menuInactive();
  }

  public menuInactive(){
    this.menu.enable(false);
  }

  public menuActive(){
    this.menu.enable(true);
  }

  public pageSignup(){
    this.navCtrl.push(SignupPage);
  }

}
